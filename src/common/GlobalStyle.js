import {StyleSheet} from 'react-native';
import Colors from '../common/Colors'

export default (GlobalStyle = StyleSheet.create({
    row:{
        flexDirection:"row"
    },
    column:{
        flexDirection:'column'
    },
    pad10:{
        padding:10
    },
    mt10:{
        marginTop:10
    },



})

);