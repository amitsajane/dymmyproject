import React from 'react';
import {StatusBar, } from 'react-native'
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';


import HomeScreen from '../screens/HomeScreen';
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
import SplashScreen from '../screens/SplashScreen';



const HomeStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
 
  <Tab.Navigator initialRouteName="HomeScreen" activeColor="#fff">
    <Tab.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarLabel: 'Home',
        tabBarColor: '#009387',
        tabBarIcon: ({color}) => (
          <Icon name="ios-home" color={color} size={26} />
        ),
     
      }}
    />
    <Tab.Screen
      name="SignUp"
      component={SignUpScreen}
      options={{
        tabBarLabel: 'SignUp',
        tabBarColor: '#009387',
        tabBarIcon: ({color}) => (
          <Icon name="person-add-outline" color={color} size={26} />
        ),
       
      }}
    />
    <Tab.Screen
      name="Sign In"
      component={SignInScreen}
      options={{
        tabBarLabel: 'Sign In',
        tabBarColor: '#009387',
        tabBarIcon: ({color}) => (
          <Icon name="person-outline" color={color} size={26} />
        ),
       
      }}
    />
    <Tab.Screen
      name="Splash"
      component={SplashScreen}
      options={{
        tabBarLabel: 'Splash',
        tabBarColor: '#009387',
        activeColor:'red',

        tabBarIcon: ({color}) => (
          <Icon name="paper-plane-outline" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTabScreen;


const HomeStackScreen = ({navigation}) => (
  <HomeStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: '#009387',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{
        title: 'Home',
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          />
        ),
      }}
    />
  </HomeStack.Navigator>
);

