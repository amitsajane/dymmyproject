import React, {Component} from 'react';

import { createDrawerNavigator, DrawerContent } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import MainTabScreen from './MainTabScreen';
import SignUpScreen from '../screens/SignUpScreen';
import HomeScreen from '../screens/HomeScreen';
import SignInScreen from '../screens/SignInScreen';
import SplashScreen from '../screens/SplashScreen';

const Drawer = createDrawerNavigator();

const  DrawerNavigator = () => {
        return  (
        <NavigationContainer >
       {/* <Drawer.Navigator drawerContent ={(props) => <DrawerContent {...props}  />} > */}
       <Drawer.Navigator initialRouteName="HomeScreen">
       <Drawer.Screen name="HomeDrawer" component={MainTabScreen} />
       {/* <Drawer.Screen name="Home" component={HomeScreen} /> */}
       <Drawer.Screen name="SignUpScreen" component={SignUpScreen} />
       <Drawer.Screen name="SignInScreen" component={SignInScreen} />
       <Drawer.Screen name="SplashScreen" component={SplashScreen} />
        </Drawer.Navigator>
       </NavigationContainer>
        );     
}



export default DrawerNavigator;