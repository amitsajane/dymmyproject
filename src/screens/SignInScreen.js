
import React, {Component} from 'react';
import { Button, View, Text } from 'react-native';

export default class SignInScreen extends Component{ 
  constructor(props) {
    super(props);

  }
  render(){
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text> SignINScreen</Text>
      <Button
        title="Go to SignUpScreen"
        onPress={() =>this.props.navigation.navigate('SplashScreen')}
      />
    </View>
  );
}
}