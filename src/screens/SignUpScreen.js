
import React, {Component} from 'react';
import { Button, View, Text } from 'react-native';

export default class SignUpScreen extends Component{ 
  render(){
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Second Screen</Text>
      <Button
        title="Go to Back"
        onPress={() => this.props.navigation.navigate('SignInScreen')}
      />
    </View>
  );
}
}