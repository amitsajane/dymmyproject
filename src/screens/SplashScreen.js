
import React, {Component} from 'react';
import { Button, View, Text } from 'react-native';

export default class SplashScreen extends Component{ 
  constructor(props) {
    super(props);

  }
  render(){
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>SplashScreen</Text>
      <Button
        title="Go to SignInScreen"
         onPress={() => this.props.navigation.navigate('SignInScreen')}
      />
    </View>
  );
}
}